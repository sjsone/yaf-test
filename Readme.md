# Yet Another F@#%ing Test

A simple test-suite without dependencies. 

## Getting started

```typescript
import { TestGroup } from "yaf-test"

const func = (a: string = 'a') => a + 'sdf'

TestGroup.Area('Simple Tests', test => {
    test('String compare', 'asdf').is('asdf')
    test('Function compare', func).is(func)
    test('Execute function', func).execute().is('asdf')
    test('Execute function + arguments', func).execute(['b']).is('bsdf')
    test('Compare Object', {a: {b: 'c'}}).equal({a: {b: 'c'}})
})
```

![Simple Tests: All tests successfull](https://gitlab.com/sjsone/yaf-test/-/raw/main/assets/simpleTestResult.png)

## Test Factory

Each factory needs a label and _item_. 

## Test Methods

Nearly all methods return the `Test` instance so it is possible to chain them

### is 

`is(what: any): Test` strictly compares (`===`)

Throws `IsNotError` 

### isLike

`isLike(what: any): Test` compares (`==`)

Throws `IsNotLikeError` 

### equal

`equal(object: Object): Test` checks if _item_ is equal to _object_ 

Throws `NotAnObject` if _item_ or _object_ is not an Object  

Throws `NotEqual` if  _item_ and _object_ are not equal

### inArray

`inArray(array: any[]): Test` checks if _item_ is in _array_

Throws `IsNotAnArray` if _array_ is not an array 

Throws `NotInArrayError` if _item_ is not in _array_

### includes

`includes(what: any): Test` checks if _item_ includes _what_

Throws `IsNotAnArray` if _item_ is not an array 

Throws `NotInArrayError` if _item_ does not include _what_

### execute

`execute(...args: any): Test` executes _item_ 

Throws `NotAFunction`  if _item_ is not a function

### executeMethod

`executeMethod(thisArg: Object, ...args: any): Test` executes _item_ with given _this_

Throws `NotAFunction`  if _item_ is not a function

### failing

`failing(clearErrors: boolean = true)`  checks if errors occured and clears them if _clearErrors_ is `true`

Throws `NotFailing` if no errors occured

### 