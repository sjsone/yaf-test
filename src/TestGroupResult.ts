import { TestGroup } from "./TestGroup";
import { Colors as C } from "./Colors";
import { Test } from "./Test";

export interface TestGroupResultPrintSettings {
    showFailing?: boolean
    showSuccessful?: boolean
    showErrors?: boolean
}
export class TestGroupResult {
    protected label: string

    protected tests: Test[]
    protected testsWithError: Test[] = []
    protected successfulTests: Test[] = []

    constructor(testGroup: TestGroup) {
        this.label = testGroup.getLabel()
        this.tests = testGroup.getTests()

        for (const test of testGroup.getTests()) {
            if (test.hasErrors()) {
                this.testsWithError.push(test)
            } else {
                this.successfulTests.push(test)
            }
        }
    }

    protected printTestGroupName() {
        console.log(`${C.Bright}${C.FgBlue}${C.Underscore}${this.label}${C.Reset}: \n`)
    }

    getLabel() {
        return this.label
    }

    getTestsWithErrors() {
        return this.testsWithError
    }

    getSuccessfulTests() {
        return this.successfulTests
    }

    getTestByLabel(label: string) {
        return this.tests.find(test => label === test.getLabel())
    }

    getTestCount() {
        return this.tests.length
    }

    getSuccessfulCount() {
        return this.successfulTests.length
    }

    getUnsuccessfulCount() {
        return this.testsWithError.length
    }

    allTestsSuccessful() {
        return this.getSuccessfulCount() === this.getTestCount()
    }

    print(settings: TestGroupResultPrintSettings = {showFailing: true, showSuccessful: true, showErrors: true}) {
        this.printTestGroupName()
        const testCount = this.tests.length

        if (this.testsWithError.length === 0) {
            console.log(`${C.FgGreen}All tests successfull ${C.Reset}`)
        } else {
            console.log(`${C.FgRed}Some tests have errors ${C.Reset}`)
        }

        console.log(`${this.successfulTests.length} of ${testCount} tests successful\n`)
        for (const test of this.tests) {
            if(!settings.showFailing && test.hasErrors() || !settings.showSuccessful && !test.hasErrors()) {
                continue
            }

            console.log(`${C.FgMagenta}${test.getLabel()}${C.Reset} was ${test.hasErrors() ? `${C.FgRed}not successful${C.Reset}` : `${C.FgGreen}successful${C.Reset}`}`)
            if(settings.showErrors) {
                for (const error of test.getErrors()) {
                    console.log(`\t${C.FgRed}${error.constructor.name}${C.Reset}: ${error.message}`)
                }
            }
            console.log('')
        }
        console.log('\n')
    }
}