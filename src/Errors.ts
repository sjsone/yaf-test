import { type } from 'os'
import { Colors as C } from './Colors'

class AbstractTestError extends Error {
    item: any
    what: any

    constructor(item: any, what: any) {
        super()
        this.item = item
        this.what = what

        this.createMessage()
    }

    toString(item: any) {
        if (typeof item === 'function') {
            return `${item.name} ${item}`
        }
        if (typeof item === 'object') {
            return JSON.stringify(item)
        }
        if (typeof item === 'string') {
            return `"${item}"`
        }
        return `${item}`
    }

    protected createMessage() {

    }
}

export class IsNotError extends AbstractTestError {
    createMessage() {
        const itemString = this.toString(this.item)
        const whatString = this.toString(this.what)
        this.message = `${C.FgCyan}${itemString}${C.Reset} is not ${C.FgBlue}${whatString}${C.Reset}`
    }
}

export class IsNotLikeError extends AbstractTestError {
    createMessage() {
        const itemString = this.toString(this.item)
        const whatString = this.toString(this.what)
        this.message = `${C.FgCyan}${itemString}${C.Reset} is not like ${C.FgBlue}${whatString}${C.Reset}`
    }
}

export class NotInArrayError extends AbstractTestError {
    createMessage() {
        const itemString = this.toString(this.item)
        const whatString = this.toString(this.what)
        this.message = `${C.FgCyan}${itemString}${C.Reset} is not in ${C.FgBlue}${whatString}${C.Reset}`
    }
}

export class IsNotAnArray extends AbstractTestError {
    constructor(what: any) {
        super(undefined, what)
    }

    createMessage() {
        const whatString = this.toString(this.what)
        this.message = `${C.FgBlue}${whatString}${C.Reset} is not an array`
    }
}

export class NotAFunction extends AbstractTestError {

    constructor(item: any, args: any[]) {
        super(item, args)
    }

    createMessage() {
        const itemString = this.toString(this.item)
        const args = this.what !== undefined ? this.what : []
        this.message = `Tried to execute non function ${C.FgCyan}${itemString}${C.Reset} with [${args.join(', ')}]`
    }
}

export class NotEqual extends AbstractTestError {

    createMessage() {
        const itemString = this.toString(this.item)
        const whatString = this.toString(this.what)
        this.message = `${C.FgCyan}${itemString}${C.Reset} is not equal to ${C.FgBlue}${whatString}${C.Reset}`
    }
}

export class NotFailing extends AbstractTestError {
    constructor() {
        super(undefined, undefined)
    }

    createMessage() {
        this.message = `The test should fail but does not`
    }
}

export class NotAnObject extends AbstractTestError {
    constructor(what: any) {
        super(undefined, what);
    }

    createMessage() {
        const whatString = this.toString(this.what)
        this.message = `${C.FgCyan}${whatString}${C.Reset} is not an ${C.FgBlue}Object${C.Reset}`
    }
}