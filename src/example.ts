import { TestGroup } from "./lib"

const funcA = (str: string = '') => str + 'asdf'
const funcB = (str: string = '') => str + '1234'

const strA = 'a_string'
const strB = 'b_string'

const asyncSetTimeout = <T>(timeout: number, ret: T) => new Promise<T>((res) => {
    setTimeout(() => res(ret), timeout)
})

class Service {
    protected data = 'service'

    run(type: string) {
        return `${type} ${this.data}`
    }
}
const service = new Service()

const succeedingResult = TestGroup.Area('All should succeed', testSucceeding => {
    testSucceeding('String compare', "asdf").is('asdf')
    testSucceeding('String compare multiple', 'asdf').is('asdf').is(funcA())
    testSucceeding('Function compare', funcA).is(funcA)
    testSucceeding('Function return string compare', funcA).execute().is('asdf')
    testSucceeding('Function argument return string compare', funcA).execute('1234').is('1234asdf')
    testSucceeding('Method execute', service.run).executeMethod(service, ['test']).is('test service')
    testSucceeding('Object compare 1D', { "a": "b" }).equal({ "a": "b" })
    testSucceeding('Object compare 2D', { "a": { "b": "c" } }).equal({ "a": { "b": "c" } })
    testSucceeding('Object compare 2D with func', { "a": { "b": funcA } }).equal({ "a": { "b": funcA } })
    testSucceeding('Array includes', ['a']).includes('a')
    testSucceeding('included in Array', 'a').inArray(['a'])
}, false)

const failingResult = TestGroup.Area('All should fail', testFailing => {
    testFailing('String compare', strB).is('asdf').failing()
    testFailing('Function compare', funcA).is(funcB).failing()
    testFailing('Execute non function', strA).execute().failing()
    testFailing('Object compare different 1D', { "a": "b" }).equal({ "a": "c" }).failing()
    testFailing('Object compare 2D with different funcs', { "a": { "b": funcA } }).equal({ "a": { "b": funcB } }).failing()
    testFailing('Array does not include', ['a']).includes('z').failing()
    testFailing('not included in Array', 'z').inArray(['a']).failing()
    testFailing('Not an Object', {}).equal('asdf').failing()
}, false)

TestGroup.GroupResultArea('Test Results', [
    succeedingResult,
    failingResult
])

TestGroup.Area('Async [Native]', async testAsync => {
    testAsync('setTimeout 0.5s', await asyncSetTimeout(500, "500ms")).is("500ms")
    testAsync('setTimeout 1.5s failing', await asyncSetTimeout(1500, "1500ms")).is("0").failing()
})
