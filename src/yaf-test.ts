#!/usr/bin/env node

import { argv } from "process";
import * as NodePath from 'path'
import * as NodeFs from 'fs'
import { TestManager } from "./TestManager";

TestManager.setRunningAsTest(true)

const filePath = argv[2]
if (!filePath) {
    console.log("Missing argument")
    process.exit(1)
}
const absoluteFilePath = NodePath.resolve(filePath)
if (!NodeFs.existsSync(absoluteFilePath)) {
    console.log(`Cannot find file ${absoluteFilePath}`)
    process.exit(1)
}

const imported = require(absoluteFilePath)
if(imported.default !== undefined && typeof imported.default === 'function') {
    imported.default()
}

if(TestManager.allGroupsSuccessful()) {
    process.exit(0)
}

for(const group of TestManager.getAllUnsuccessfulGroups()) {
    group.getResult()?.print()
}
process.exit(1)