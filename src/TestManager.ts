import { TestGroup } from "./TestGroup"

class TestManager {
    protected testGroups: TestGroup[] = []
    protected runningAsTest: boolean = false

    addTestGroup(testGroup: TestGroup) {
        this.testGroups.push(testGroup)
    }

    setRunningAsTest(runningAsTest: boolean) {
        this.runningAsTest = runningAsTest
    }

    isRunningAsTest() {
        return this.runningAsTest
    }

    getGroups() {
        return this.testGroups
    }

    allGroupsSuccessful() {
        for(const group of this.testGroups) {
            const result = group.getResult()
            if(result !== undefined) {
                if(!result.allTestsSuccessful()) {
                    return false
                }
            }
        }
        return true
    }

    getAllUnsuccessfulGroups() {
        return this.testGroups.filter(group => !group.getResult()?.allTestsSuccessful())
    }
}

const testManager = new TestManager()
export { testManager as TestManager, TestManager as testManagerClass }