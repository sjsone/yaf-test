import { IsNotError, IsNotLikeError, IsNotAnArray, NotInArrayError, NotAFunction, NotEqual, NotFailing, NotAnObject } from "./Errors"
import { TestGroup } from "./TestGroup"

const compareObject = (a: { [key: string]: any }, b: { [key: string]: any }) => {
    for (const key of Object.getOwnPropertyNames(a)) {
        const elementA = a[key]
        const elementB = b[key]
        if (typeof elementA === 'object') {
            if (typeof elementB !== 'object') {
                return false
            }
            if (!compareObject(elementA, elementB)) {
                return false;
            }
        } else if (elementA !== elementB) {
            return false
        }
    }
    return true
}

export class Test {
    protected label: string
    protected statementExecuted: boolean = false
    protected statement: any
    protected statementResult: any

    protected errors: Error[] = []

    protected testGroup: TestGroup

    constructor(testGroup: TestGroup, label: string, statement: any) {
        this.testGroup = testGroup
        this.label = label
        this.statement = statement
    }

    protected getItem() {
        return this.statementExecuted ? this.statementResult : this.statement
    }

    execute(...args: any) {
        if (typeof this.statement !== 'function') {
            this.errors.push(new NotAFunction(this.statement, args))
        } else {
            this.statementResult = this.statement(...args)
            this.statementExecuted = true
        }
        return this
    }

    executeMethod(thisArg: Object, ...args: any) {
        if (typeof this.statement !== 'function') {
            this.errors.push(new NotAFunction(this.statement, args))
        } else {
            this.statementResult = (<Function>this.statement).apply(thisArg, args)
            this.statementExecuted = true
        }
        return this
    }

    is(what: any) {
        const item = this.getItem()
        if (item !== what) {
            this.errors.push(new IsNotError(item, what))
        }
        return this
    }

    isLike(what: any) {
        const item = this.getItem()
        if (item != what) {
            this.errors.push(new IsNotLikeError(item, what))
        }
        return this
    }

    equal(object: Object) {
        const item = this.getItem()

        if (typeof object !== 'object') {
            this.errors.push(new NotAnObject(object))
            return this
        }

        if (typeof item !== 'object') {
            this.errors.push(new NotAnObject(item))
            return this
        }

        if(!compareObject(item, object)) {
            this.errors.push(new NotEqual(item, object))
        }
        
        return this

    }

    inArray(array: any[]) {
        const item = this.getItem()

        if (!Array.isArray(array)) {
            this.errors.push(new IsNotAnArray(array))
            return this
        }

        for (const element of array) {
            if (element === item) {
                return this
            }
        }
        this.errors.push(new NotInArrayError(item, array))
        return this
    }

    includes(what: any) {
        const item = this.getItem()
        if (!Array.isArray(item)) {
            this.errors.push(new IsNotAnArray(item))
            return this
        }
        if (!item.includes(what)) {
            this.errors.push(new NotInArrayError(what, item))
        }
        return this
    }

    failing(clearErrors: boolean = true) {
        if (this.errors.length === 0) {
            this.errors.push(new NotFailing())
        } else if (clearErrors) {
            this.errors = []
        }
    }

    getLabel() {
        return this.label
    }

    hasErrors() {
        return this.errors.length !== 0
    }

    getErrors() {
        return this.errors
    }

    printTestResult() {
        if (!this.hasErrors()) {
            console.log(`"${this.label}" was successful`)
        } else {
            console.log(`"${this.label}" has errors: `)
            for (const error of this.getErrors()) {
                console.log(`\t - ${error.message}`)
            }
        }
    }
}