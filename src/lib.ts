import { TestGroup } from "./TestGroup"

class DefaultTestGroup extends TestGroup {
    printTestGroupName() { }
}

const defaultTestGroup = new DefaultTestGroup('default')

const test = defaultTestGroup.createTest()
export { test }
export * from "./Colors"
export * from "./TestGroup"
export * from "./Errors"
export { Test as TestClass } from "./Test"