import { Test } from "./Test";
import { TestGroupResult, TestGroupResultPrintSettings } from "./TestGroupResult";
import { TestManager } from "./TestManager";

export type TestFactory = {
    (label: string, statement: any): Test;
    Result: () => void
}
export type AreaFunction = (testFactory: TestFactory) => void

export class TestGroup {
    protected tests: Test[] = []
    protected label: string
    protected result?: TestGroupResult

    constructor(label: string) {
        this.label = label

        TestManager.addTestGroup(this)
    }

    createTest() {
        const testFactory: TestFactory = (label: string, statement: any) => {
            const testInstance = new Test(this, label, statement)
            this.tests.push(testInstance)
            return testInstance
        }
        testFactory.Result = () => new TestGroupResult(this)
        return testFactory
    }

    getTests() {
        return this.tests
    }

    getResult() {
        return this.result
    }

    getLabel() {
        return this.label
    }

    static Create(label: string) {
        const group = new TestGroup(label)
        return group.createTest()
    }

    static async Area(label: string, area: AreaFunction, print: boolean = true, printSettings?: TestGroupResultPrintSettings) {
        const group = new TestGroup(label)
        const test = group.createTest()
        await area(test)
        const result = new TestGroupResult(group)
        if (print && !TestManager.isRunningAsTest()) {
            result.print(printSettings)
        }
        group.result = result
        return result
    }

    static GroupResultArea(label: string, testGroupResults: Array<TestGroupResult|Promise<TestGroupResult>>, print: boolean = true) {
        return TestGroup.Area(label, async test => {
            for (const testGroupResult of testGroupResults) {
                const awaitedTestGroupResult = await testGroupResult
                const allTestsSuccessful = awaitedTestGroupResult.allTestsSuccessful()

                if (!allTestsSuccessful) {
                    awaitedTestGroupResult.print({ showFailing: true, showSuccessful: false, showErrors: true })
                }

                test(awaitedTestGroupResult.getLabel(), allTestsSuccessful).is(true)
            }
        }, print, { showFailing: true, showSuccessful: true, showErrors: false })
    }
}